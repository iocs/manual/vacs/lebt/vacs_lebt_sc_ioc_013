#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_leyboldtd20
#
require vac_ctrl_leyboldtd20,3.5.1


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_leyboldtd20_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: LEBT-010:Vac-VEPT-02100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_ctrl_leyboldtd20_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEPT-02100, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4005")

#
# Device: LEBT-010:Vac-VPT-02100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_pump_leyboldtd20_vpt.iocsh", "DEVICENAME = LEBT-010:Vac-VPT-02100, CONTROLLERNAME = LEBT-010:Vac-VEPT-02100")

#
# Device: LEBT-010:Vac-VEPT-03100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_ctrl_leyboldtd20_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEPT-03100, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4006")

#
# Device: LEBT-010:Vac-VPT-03100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_pump_leyboldtd20_vpt.iocsh", "DEVICENAME = LEBT-010:Vac-VPT-03100, CONTROLLERNAME = LEBT-010:Vac-VEPT-03100")

#
# Device: LEBT-010:Vac-VEPT-06100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_ctrl_leyboldtd20_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEPT-06100, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4007")

#
# Device: LEBT-010:Vac-VPT-06100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_pump_leyboldtd20_vpt.iocsh", "DEVICENAME = LEBT-010:Vac-VPT-06100, CONTROLLERNAME = LEBT-010:Vac-VEPT-06100")

#
# Device: LEBT-010:Vac-VEPT-07100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_ctrl_leyboldtd20_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEPT-07100, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4008")

#
# Device: LEBT-010:Vac-VPT-07100
# Module: vac_ctrl_leyboldtd20
#
iocshLoad("${vac_ctrl_leyboldtd20_DIR}/vac_pump_leyboldtd20_vpt.iocsh", "DEVICENAME = LEBT-010:Vac-VPT-07100, CONTROLLERNAME = LEBT-010:Vac-VEPT-07100")
