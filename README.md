# IOC for LEBT vacuum turbopumps

## Used modules

*   [vac_ctrl_leyboldtd20](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_leyboldtd20)


## Controlled devices

*   LEBT-010:Vac-VEPT-02100
    *   LEBT-010:Vac-VPT-02100
*   LEBT-010:Vac-VEPT-03100
    *   LEBT-010:Vac-VPT-03100
*   LEBT-010:Vac-VEPT-06100
    *   LEBT-010:Vac-VPT-06100
*   LEBT-010:Vac-VEPT-07100
    *   LEBT-010:Vac-VPT-07100
